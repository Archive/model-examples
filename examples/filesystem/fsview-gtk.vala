class App {
  MainLoop main_loop;

  bool window_destroyed (Gdk.Event event) {
    main_loop.quit ();
    return false;
  }

  void set_integer (Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.TreeModel model, Gtk.TreeIter iter) {
        int val;
        model.get (iter, 3, out val, -1);
        cell.set ("text", val.to_string ());
  }

  internal void run (File location) {
    string[] keys = { "contents", "name", "user", "size", "type" };
    Type[] types = { typeof (Model.List),
                     typeof (string), typeof (string),
                     typeof (int), typeof (string) };
    var d = new FS.Directory (location);
    var v = new Gtk.TreeView ();
    var s = new Gtk.ScrolledWindow (null, null);
    var w = new Gtk.Window (Gtk.WindowType.TOPLEVEL);

    v.set_model (new ModelGtk.TreeModel (d, 5, keys, types, false));
    v.insert_column_with_attributes (0, "Name", new Gtk.CellRendererText (),
                                     "text", 1);
    v.insert_column_with_attributes (1, "User", new Gtk.CellRendererText (),
                                     "text", 2);
    v.insert_column_with_data_func (1, "Size", new Gtk.CellRendererText (),
                                    set_integer);
    v.insert_column_with_attributes (1, "Type", new Gtk.CellRendererText (),
                                     "text", 4);
    s.add (v);
    w.add (s);

    w.destroy_event.connect (window_destroyed);
    w.set_default_size (800, 600);
    w.show_all ();

    main_loop = new MainLoop (null, false);
    main_loop.run ();
  }
}

void main (string[] args) {
  var app = new App ();

  Gtk.init (ref args);

  if (args.length == 2 && args[1] != "-uri") {
    app.run (File.new_for_path (args[1]));
  } else if (args.length == 3 && args[1] == "--") {
    app.run (File.new_for_path (args[2]));
  } else if (args.length == 3 && args[1] == "-uri") {
    app.run (File.new_for_uri (args[2]));
  } else {
    print ("usage: %s file\n       %s -uri uri\n", args[0], args[0]);
  }
}
