namespace FS {
  public class File : Model.SimpleDictionary {
    internal string name;
    FileMonitor? monitor;
    GLib.File file;
    ulong watch_id;

    static void monitor_changed (FileMonitor monitor, GLib.File file,
                                 GLib.File? other_file,
                                 FileMonitorEvent event, File this_file) {
      assert (this_file.monitor == monitor);

      if ((event == FileMonitorEvent.CHANGED ||
           event == FileMonitorEvent.ATTRIBUTE_CHANGED) &&
          this_file.file.equal (file)) {
        this_file.update_info.begin ();
      }
    }

    async void update_info () {
      try {
        var info = yield file.query_info_async ("standard::size," +
                                                "standard::content-type," +
                                                "owner::user",
                                                0, 0, null);

        set_integer ("size", (int) info.get_size (), true);
        set_string ("type", info.get_content_type (), true);
        set_string ("user", info.get_attribute_string ("owner::user"), true);

        if (info.get_content_type () == "inode/directory") {
          set ("contents", new Directory (file), true);
        } else {
          set ("contents", null, true);
        }
      } catch (Error e) {
        /* file probably just disappeared in the meantime.  do nothing */
      }
    }

    ~File () {
      if (watch_id != 0) {
        SignalHandler.disconnect (monitor, watch_id);
      }
    }

    public File (GLib.File file, FileMonitor? monitor) {
      this.file = file;
      this.name = (!) file.get_basename ();
      this.monitor = monitor;

      set_string ("name", name, false);
      set ("contents", null);
      set ("size", null);
      set ("type", null);
      set ("user", null);

      update_info.begin ();

      if (monitor != null) {
        this.watch_id = Signal.connect (monitor, "changed",
                                        (Callback) monitor_changed, this);
      }
    }
  }

  public class Directory : Model.AbstractSortedList
  {
    FileMonitor? monitor;
    ulong watch_id;
    GLib.File file;

    ~Directory () {
      if (watch_id != 0) {
        SignalHandler.disconnect (monitor, watch_id);
      }
    }

    override void free_key (Model.AbstractSortedList.Key key) {
    }

    override int compare (Model.AbstractSortedList.ConstKey a,
                          Model.AbstractSortedList.ConstKey b) {
      return strcmp ((string) a, (string) b);
    }

    override void create_item (ulong index,
                               Model.AbstractSortedList.ConstKey key,
                               out Model.AbstractSortedList.Key new_key,
                               out Model.Object new_object) {
      var child = new File (file.get_child ((string) key), monitor);
      weak string name = child.name;
      new_object = child;
      new_key = (Model.AbstractSortedList.Key) name;
    }

    override void warning (ulong index, Model.AbstractSortedList.ConstKey key,
                           char mode, ulong current_index,
                           Model.Object current_value) {
      assert_not_reached ();
    }

    static int compare_strings (void *a, void *b) {
      string **a_str = a;
      string **b_str = b;

      return strcmp (*a_str, *b_str);
    }

    async void enumerate () {
      string[] names = null;

      try {
        var enumerator =
          yield file.enumerate_children_async ("standard::name", 0, 0, null);

        do {
          var files = yield enumerator.next_files_async (2000, 0, null);
          names = new string[0];

          foreach (var file in files) {
            names += file.get_name ();
          }

          Posix.qsort ((void *) names, names.length, sizeof (void *),
                       (Posix.compar_fn_t) compare_strings);

          merge ("i", (Model.AbstractSortedList.ConstKey[]) names);
        } while (names.length == 5);
      } catch (Error e) {
      }
    }

    static void monitor_changed (FileMonitor monitor, GLib.File file,
                                 GLib.File? other_file,
                                 FileMonitorEvent event, Directory this_dir) {
      string mode = "";

      assert (this_dir.monitor == monitor);

      switch (event) {
        case FileMonitorEvent.CREATED:
          mode = "i";
          break;
        case FileMonitorEvent.DELETED:
          mode = "d";
          break;
        default:
          return;
      }

      string[] names = new string[] { file.get_basename () };
      this_dir.merge (mode, (Model.AbstractSortedList.ConstKey[]) names);
    }

    public Directory (GLib.File file) {
      Object ();

      this.file = file;

      try {
        this.monitor = file.monitor_directory (0, null);
        this.watch_id = Signal.connect (monitor, "changed",
                                        (Callback) monitor_changed, this);
      } catch (GLib.IOError e) {
        this.monitor = null;
        this.watch_id = 0;
      }

      enumerate.begin ();
    }
  }
}
