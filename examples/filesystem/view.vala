protected abstract class Widget {
  protected weak Widget? parent;

  protected virtual void invalidate () {
    if (parent != null) {
      ((!) parent).invalidate ();
    }
  }

  protected abstract void draw ();
}

protected class Cell : Widget {
  Model.Reference reference;
  string displayed;
  ulong watch_id;

  static void _changed (Model.Reference reference, Cell this_cell) {
    this_cell.changed (reference);
  }

  void changed (Model.Reference reference) {
    assert (this.reference == reference);

    var value = reference.get_value ();

    if (value == null) {
      displayed = "                      ";
    } else if (value is Model.String) {
      displayed = " %-20.20s ".printf ((value as Model.String).get ());
    } else if (value is Model.Integer) {
      displayed = " %20d ".printf ((value as Model.Integer).get ());
    } else if (value is Model.Float) {
      displayed = " %20.13g ".printf ((value as Model.Float).get ());
    } else if (value is Model.Boolean) {
      displayed = " %-20s ".printf ((value as Model.Boolean).get () ?
                                    "true" : "false");
    } else {
      assert_not_reached ();
    }

    invalidate ();
  }

  protected Cell (Model.Reference reference) {
    this.reference = reference;
    this.watch_id = Signal.connect (reference, "changed",
                                    (Callback) _changed, this);
    changed (reference);
  }

  ~Cell () {
    SignalHandler.disconnect (reference, watch_id);
  }

  override void draw () {
    print ("%s", displayed);
  }
}

class Row : Widget {
  Cell[] cells;

  protected Row (Model.Dictionary dictionary, string[] columns) {
    foreach (var column in columns) {
      var cell = new Cell (dictionary.get_reference (column));
      cell.parent = this;
      cells += cell;
    }
  }

  override void draw () {
    foreach (var cell in cells) {
      print ("|");
      cell.draw ();
    }
    print ("|\n");
  }
}

public class ListView : Widget {
  string[] columns;
  Model.List list;
  ulong watch_id;
  Row[] rows;

  static void _changed (Model.List list, ulong position, ulong removed,
                        ulong inserted, bool more, ListView this_view) {
    this_view.changed (list, position, removed, inserted, more);
  }

  void changed (Model.List list, ulong position, ulong removed,
                ulong inserted, bool more) {
    var new_rows = new Row[0];

    for (ulong i = 0; i < position; i++)
      new_rows += rows[i];

    for (ulong i = position; i < position + inserted; i++)
      {
        var row = new Row (list.get_child (i) as Model.Dictionary, columns);
        row.parent = this;
        new_rows += row;
      }

    for (ulong i = position + removed; i < rows.length; i++)
      new_rows += rows[i];

    rows = (owned) new_rows;

    invalidate ();
  }

  public ListView (Model.List list, string[] columns) {
    this.columns = columns;
    this.list = list;

    for (ulong i = 0; i < list.n_children (); i++) {
      var row = new Row (list.get_child (i) as Model.Dictionary, columns);
      row.parent = this;
      rows += row;
    }

    watch_id = Signal.connect (list, "changed", (Callback) _changed, this);
  }

  ~ListView () {
    SignalHandler.disconnect (list, watch_id);
  }

  void bar () {
    foreach (var column in columns) {
      print ("+----------------------");
    }
    print ("+\n");
  }

  override void draw () {
    bar ();

    foreach (var column in columns) {
      print ("| %-20s ", column);
    }
    print ("|\n");

    bar ();

    foreach (var row in rows) {
      row.draw ();
    }

    bar ();
  }
}

public class Toplevel : Widget {
  Widget widget;
  uint id;

  public Toplevel (Widget widget) {
    this.widget = widget;
    widget.parent = this;
    draw ();
  }

  override void invalidate () {
    if (id == 0) {
      id = Idle.add (idle_draw);
    }
  }

  bool idle_draw () {
    draw ();
    id = 0;
    return false;
  }

  override void draw () {
    print ("\033[2J\033[H");
    widget.draw ();
  }
}
