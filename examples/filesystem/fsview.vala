class App {
  MainLoop main_loop;
  Toplevel t;

  bool stdin_ready (IOChannel channel, IOCondition condition) {
    main_loop.quit ();
    return false;
  }

  internal void run (File location) {
    var d = new FS.Directory (location);
    var l = new ListView (d, new string[] { "name", "size", "user", "type" });
    t = new Toplevel (l);

    var stdin = new IOChannel.unix_new (0);
    stdin.add_watch (IOCondition.IN, stdin_ready);

    main_loop = new MainLoop (null, false);
    main_loop.run ();
  }
}

void main (string[] args) {
  var app = new App ();

  if (args.length == 2 && args[1] != "-uri") {
    app.run (File.new_for_path (args[1]));
  } else if (args.length == 3 && args[1] == "--") {
    app.run (File.new_for_path (args[2]));
  } else if (args.length == 3 && args[1] == "-uri") {
    app.run (File.new_for_uri (args[2]));
  } else {
    print ("usage: %s file\n       %s -uri uri\n", args[0], args[0]);
  }
}
