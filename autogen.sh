#!/bin/bash

set -e

if [ "$1" = "clean" ]; then
  rm -f aclocal.m4 configure missing install-sh \
        depcomp config.guess config.sub \
        `find . -name Makefile.in` compile
  rm -rf autom4te.cache      
  exit
fi

aclocal $ACLOCAL_FLAGS
automake --add-missing --foreign
autoconf

CFLAGS=${CFLAGS=-ggdb -Werror}
LDFLAGS=${LDFLAGS=-Wl,-O1}
export CFLAGS LDFLAGS

./configure "$@"
